<?php

/**
 * @file
 */

/**
 * Implement hook_rules_action_info().
 */
function linkedin_autopost_rules_action_info() {
  $actions = array(
    'linkedin_post_action' => array(
      'label' => t('Send to linkedin'),
      'group' => t('linkedin'),
      'module' => 'linkedin_autopost',
      'parameter' => array(
        'node' => array('type' => 'node', 'label' => t('Current Node')),
      ),
    ),
  );
  return $actions;
}

/**
 * Implements hook_rules_condition_info().
 */
function linkedin_autopost_rules_condition_info() {
  return array(
    'linkedin_autopost_node_published' => array(
      'group' => t('linkedin'),
      'label' => t('Current node published to linkedin'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Validate node if status published'),
        ),
      ),
    ),
  );
}

/**
 * Callback function for custom condition info.
 */
function linkedin_autopost_node_published($node) {
  $linked = isset($node->field_post_ot_linkedin['und'][0]['value']) ? $node->field_post_ot_linkedin['und'][0]['value'] : 0;
  if ($linked) {
    $new = $node->status;
    $old = isset($node->original) ? $node->original->status : 0;
    if ($old == 0 && $new == 1) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * The action linkedin.
 */
function linkedin_post_action($node) {

  libraries_load('linkedin-api-client');
  $linkedIn = new Happyr\LinkedIn\LinkedIn(variable_get('linkedin_autopost_clien_id', ''), variable_get('linkedin_autopost_clien_secret_key', ''));
  $linkedIn->setAccessToken(variable_get('linkedin_token', FALSE));

  if ($linkedIn->isAuthenticated()) {

    $url = url('node/' . $node->nid, array('absolute' => TRUE));
    $text = isset($node->body["und"][0]["value"]) ? $node->body["und"][0]["value"] : '';
    $fid = isset($node->field_image["und"][0]["fid"]) ? $node->field_image["und"][0]["fid"] : FALSE;
    $uri = $fid ? file_load($fid)->uri : FALSE;

    $post_fields = array(
      'json' =>
        array(
          'content' => array(
            'title' => $node->title,
            'description' => $text,
            'submitted-url' => $url,
          ),
          'visibility' => array(
            'code' => 'anyone',
          ),
        ),
    );

    // Add a picture from field_image.
    if ($uri) {
      $post_fields['json']['content']['submitted-image-url'] = image_style_url('large', $uri);
    }

    // Change the fields before sending.
    drupal_alter('linkedin_autopost', $post_fields);

    $url = variable_get('linkedin_autopost_list', 0) ? 'v1/companies/5336119/shares' : 'v1/people/~/shares';
    $result = $linkedIn->post($url, $post_fields);

    watchdog('LinkedIn API', '<pre>Output: @Output</pre>', array(
      '@Output' => print_r($result, TRUE),
    ), WATCHDOG_NOTICE);
  }
}
