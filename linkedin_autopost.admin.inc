<?php

/**
 * @file
 */

/**
 * Linkedin settings form.
 */
function linkedin_autopost_settings() {
  global $base_url;

  $form = array();

  $check_lib = libraries_load('linkedin-api-client');
  if (!$check_lib['loaded']) {
    drupal_set_message(t('Library could not be loaded. 
    Please install <a href="https://github.com/Happyr/LinkedIn-API-client" target="blank">LinkedIn API client</a>. </br> 
    Create the folder libraries/linkedin-api-client, then go to the folder and execute the command:</br> 
    composer require php-http/curl-client guzzlehttp/psr7 php-http/message happyr/linkedin-api-client'), 'error');
  }

  $check = strlen((variable_get('linkedin_autopost_clien_id', '')) == 0 || strlen(variable_get('linkedin_autopost_clien_secret_key', '')) == 0) ? TRUE : FALSE;
  if ($check) {
    drupal_set_message(t('Create a !link and set the Clien ID and Secret key for the App created.', array(
      '!link' => l(t('Linked app'), 'https://www.linkedin.com/developer/apps')
    )), 'warning');
  }

  $form['linkedin_autopost_clien_id'] = array(
    '#title' => t('Clien ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('linkedin_autopost_clien_id', ''),
  );
  $form['linkedin_autopost_clien_secret_key'] = array(
    '#title' => t('Secret key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('linkedin_autopost_clien_secret_key', ''),
  );
  $form['linkedin_autopost_list'] = array(
    '#type' => 'checkbox',
    '#title' => t('Company'),
    '#default_value' => variable_get('linkedin_autopost_list', 0),
  );
  $form['linkedin_autopost_clien_company_id'] = array(
    '#title' => t('Company ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('linkedin_autopost_clien_company_id', ''),
    '#states' => array(
      'invisible' => array(
        ':input[name="linkedin_autopost_list"]' => array('checked' => FALSE),
      ),
    ),
  );

  if ($check_lib['loaded']) {
    $linkedIn = new Happyr\LinkedIn\LinkedIn(variable_get('linkedin_autopost_clien_id', ''), variable_get('linkedin_autopost_clien_secret_key', ''));
    $url = $linkedIn->getLoginUrl();
    $form['linked_url'] = array(
      '#markup' => l(t('Login with LinkedIn'), $url),
      '#weight' => 50,
    );
  }

  if (isset($_GET['code'])) {
    variable_set('linkedin_code', $_GET['code']);
    $data = array(
      'grant_type' => 'authorization_code',
      'code' => $_GET['code'],
      'redirect_uri' => $base_url . '/admin/config/content/linkedin_autopost',
      'client_id' => variable_get('linkedin_autopost_clien_id', ''),
      'client_secret' => variable_get('linkedin_autopost_clien_secret_key', ''),
    );

    $options = array(
      'method' => 'POST',
      'data' => drupal_http_build_query($data),
      'timeout' => 15,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );

    $result = drupal_http_request('https://www.linkedin.com/oauth/v2/accessToken', $options);
    $token = isset($result->data) ? json_decode($result->data) : FALSE;
    $tokenKey = isset($token->access_token) ? $token->access_token : FALSE;
    if (isset($tokenKey)) {
      variable_set('linkedin_token', $tokenKey);
      drupal_goto(current_path());
    }
  }

  if (variable_get('linkedin_token', FALSE)) {
    $form['linked_token'] = array(
      '#markup' => '<div>Login token ' . variable_get('linkedin_token', FALSE) . '</div>',
      '#weight' => 50,
    );
    $linkedIn->setAccessToken(variable_get('linkedin_token', FALSE));
  }

  $form['linkedin_info'] = array(
    '#markup' => 'Linkedin <b>redirect</b> url - ' . $base_url . request_uri() . '</br>',
  );

  if ($check_lib['loaded'] && $linkedIn->isAuthenticated()) {
    $user = $linkedIn->get('v1/people/~:(firstName,lastName)');
    $form['linked_user'] = array(
      '#markup' => '<div>' . "Welcome " . $user['firstName'] . '</div>',
      '#weight' => 50,
    );
  }

  return system_settings_form($form);
}
